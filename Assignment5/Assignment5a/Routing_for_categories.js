/* A separate controller and factory is defined for each of the category. The factories are injected into their respective controllers. The functionalities of the controllers remain the same as specified in Assignment 4a*/

angular.module("myApp",['ngRoute'])

.config(function($routeProvider) {
    $routeProvider
        .when('/tech',
              {
                templateUrl: 'book_list.html',
                controller: "TechbookCtrl"
              })
        
        .when('/nontech',
              {
                templateUrl: 'book_list.html',
                controller: "NonTechbookCtrl"
              })
        
    
})


.factory("NonTechbook",function() {
    var books = {};
    books.list = [ 
    
    {
        "id" : "0",
       "imageUrl": "images/catching_fire.jpg", 
        "book_name": "Catching Fire", 
        "author_name": "Suzzane collins",
        "price" : "499",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":3
    }, 
    { 
        "id" : "1",
        "imageUrl": "images/divergent.jpg", 
        "book_name": "Divergent", 
        "author_name": "Veronica Roth",
        "price" : "399",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":2
    }, 
    {
        "id" : "2",
        "imageUrl": "images/harry_potter.jpg", 
        "book_name": "Harry Potter and the Sorcerer's stone", 
        "author_name": "J.K.Rowling",
        "price" : "399",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":0
    }
    
];
    return books;
})

.factory("Techbook",function() {
    var books = {};
    books.list = [ 
    {
        "id" : "0",
        "imageUrl": "images/comp_ref.jpg", 
        "book_name": "The Complete Reference for C++", 
        "author_name": "Herb Schildt",
        "price" : "699",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":4
    }, 
    
    {
        "id" : "1",
        "imageUrl": "images/mean_machine.jpg", 
        "book_name": "The MEAN machine", 
        "author_name": "Chris Sevilleja",
        "price" : "699",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":0
    }
];
    return books;
})
              
.controller("TechbookCtrl",ctrl($scope, )
    


.controller("NonTechbookCtrl",function($scope,NonTechbook) {
    
    $scope.books = NonTechbook;
     $scope.update= function(bookId) {
         for (i in $scope.books.list)
         {
              //console.log("looping");
             if($scope.books.list[i].id == bookId)
                {
                  $scope.books.list[i].check=$scope.books.list[i].check == false?true:false;
                  $scope.books.list[i].stock++;
                }
        }
     }
    
})

.directive('bookdirective', function() {
    return {
        restrict: "E",
        templateUrl: "Routing_for_categories.html"
    }
});

  function ctrl($scope,Techbook) {
        $scope.books = Techbook;
     $scope.update= function(bookId) {
         for (i in $scope.books.list)
         {
             // console.log("looping");
             if($scope.books.list[i].id == bookId)
                {
                  $scope.books.list[i].check=$scope.books.list[i].check == false?true:false;
                  $scope.books.list[i].stock++;
                }
        }
     }
   
    
}      