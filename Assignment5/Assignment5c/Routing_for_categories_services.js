/* A separate controller is defined for each of the category and for updating the array of objects data. The service holds the list of books and this is injected into the factory. The factory that is injected into the controllers holds the functions for searching through the array of objects as well as for updating it. The functionalities of the controllers is to call the search functions when the user searches for a book and accordingly determine whether to update the list or not. The UpdateCtrl is specifically used for the updating process. */

angular.module("myApp",['ngRoute'])

.config(function($routeProvider) {
    $routeProvider
        .when('/tech',
              {
                templateUrl: 'book_list.html',
                controller: "TechbookCtrl"
              })
        
        .when('/nontech',
              {
                templateUrl: 'book_list.html',
                controller: "NonTechbookCtrl"
              })
    
        .when('/update',
              {
                templateUrl: 'update.html',
                controller: "updateCtrl"
              })
    
})


.service("book",function() {
    
    this.Techlist = [ 
    {
        "book_name": "The Complete Reference for C++", 
        "author_name": "Herb Schildt",
        "price" : "699",
        "publisher" : "ABC publishers"
    }, 
    
    {
         
        "book_name": "The MEAN machine", 
        "author_name": "Chris Sevilleja",
        "price" : "699",
        "publisher" : "ABC publishers"
    }
];
    
    this.NonTechlist = [ 
    
    {
        
        "book_name": "Catching Fire", 
        "author_name": "Suzzane collins",
        "price" : "499",
        "publisher" : "ABC publishers"
    }, 
    { 
        
        "book_name": "Divergent", 
        "author_name": "Veronica Roth",
        "price" : "399",
        "publisher" : "ABC publishers"
    }, 
    {
        
        "book_name": "Harry Potter and the Sorcerer's stone", 
        "author_name": "J.K.Rowling",
        "price" : "399",
        "publisher" : "ABC publishers"
    }
];
    
})

.factory("search_update_book_factory", function(book) {
    
    var search_update_book_factory = {};
    
    search_update_book_factory.search_book = function(search, category)
    {
       
        if(category == "tech")
            for(i in book.Techlist)
            {
                
                if(book.Techlist[i].book_name == search)
                { 
                    
                    return book.Techlist[i]
                }      
                
            }
        
        else
            for(i in book.NonTechlist)
            {
                
                if(book.NonTechlist[i].book_name == search)
                    return book.NonTechlist[i]
                    
            }
        
        return ("not available");
        
    }
    
    
    search_update_book_factory.update_book = function(category,name,author_name,price,publisher)
    {
    
        if(category == "Technical" || category == "technical")
        {
            
            book.Techlist.push({'book_name' : name,
                                'author_name' : author_name,
                                'price' : price,
                                'publisher' : publisher })
            
        }
        
        else
        {
            
            book.NonTechlist.push({'book_name' : name,
                                'author_name' : author_name,
                                'price' : price,
                                'publisher' : publisher })
            
        }
        return;
    }
    
    return search_update_book_factory;
    
})
              
.controller("TechbookCtrl",function($scope, search_update_book_factory) {
        $scope.value = "Search For Technical Books"
        $scope.update_var = false;
        $scope.books = {};
        $scope.search_books = search_update_book_factory;
        $scope.$watch("search",function(val) {
            if(!val) return;
        })
        
        $scope.search_book_func = function() {
            $scope.books = $scope.search_books.search_book($scope.search,"tech");
            
            if ($scope.books == "not available")
        {
            
            $scope.update_var = true;
            
        }
       
        }
})
    

.controller("NonTechbookCtrl",function($scope,search_update_book_factory) {

    
        $scope.books = {};
        $scope.search_books = search_update_book_factory;
        $scope.$watch("search",function(val) {
            if(!val) return;
        })
        
        $scope.search_book_func = function() {
            $scope.books = $scope.search_books.search_book($scope.search,"nontech");
            
            if ($scope.books == "not available")
        {
            
            $scope.update_var = true;
            
        }
       
        }
    
})


.controller("updateCtrl", function ($scope,$location, search_update_book_factory) {
    
    $scope.update_books = search_update_book_factory;
    
    $scope.update_book_func = function() 
    {
        $scope.update_books.update_book($scope.cat,$scope.name,$scope.author_name,$scope.price,$scope.pub_name);
        
        if($scope.cat == "Technical" || $scope == "technical")
            $location.path("/tech");
        else
            $location.path("/nontech");
    
    }
    
    
})


.directive('bookdirective', function() {
    return {
        restrict: "E",
        templateUrl: "Routing_for_categories_services.html"
    }
});

        