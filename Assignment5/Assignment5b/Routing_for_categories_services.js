/* A separate controller is defined for each of the category. The service holds the list of books and this is injected into the controllers. The functionalities of the controllers remain the same as specified in Assignment 4a*/

angular.module("myApp",['ngRoute'])

.config(function($routeProvider) {
    $routeProvider
        .when('/tech',
              {
                templateUrl: 'book_list.html',
                controller: "TechbookCtrl"
              })
        
        .when('/nontech',
              {
                templateUrl: 'book_list.html',
                controller: "NonTechbookCtrl"
              })
        
    
})


.service("book",function() {
    
    this.Techlist = [ 
    {
        "id" : "0",
        "imageUrl": "images/comp_ref.jpg", 
        "book_name": "The Complete Reference for C++", 
        "author_name": "Herb Schildt",
        "price" : "699",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":4
    }, 
    
    {
        "id" : "1",
        "imageUrl": "images/mean_machine.jpg", 
        "book_name": "The MEAN machine", 
        "author_name": "Chris Sevilleja",
        "price" : "699",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":0
    }
];
    
    this.NonTechlist = [ 
    
    {
        "id" : "0",
       "imageUrl": "images/catching_fire.jpg", 
        "book_name": "Catching Fire", 
        "author_name": "Suzzane collins",
        "price" : "499",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":3
    }, 
    { 
        "id" : "1",
        "imageUrl": "images/divergent.jpg", 
        "book_name": "Divergent", 
        "author_name": "Veronica Roth",
        "price" : "399",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":2
    }, 
    {
        "id" : "2",
        "imageUrl": "images/harry_potter.jpg", 
        "book_name": "Harry Potter and the Sorcerer's stone", 
        "author_name": "J.K.Rowling",
        "price" : "399",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":0
    }
];
    
})


              
.controller("TechbookCtrl",function($scope,book) {
        $scope.books = book.Techlist;
     $scope.update= function(bookId) {
         for (i in $scope.books)
         {
              console.log("looping");
             if($scope.books[i].id == bookId)
                {
                  $scope.books[i].check=$scope.books[i].check == false?true:false;
                  $scope.books[i].stock++;
                }
        }
     }
   
    
})
    

.controller("NonTechbookCtrl",function($scope,book) {

    $scope.books = book.NonTechlist;
     $scope.update= function(bookId) {
         for (i in $scope.books)
         {
              console.log("looping");
             if($scope.books[i].id == bookId)
                {
                  $scope.books[i].check=$scope.books[i].check == false?true:false;
                  $scope.books[i].stock++;
                }
        }
     }
    
})

.directive('bookdirective', function() {
    return {
        restrict: "E",
        templateUrl: "Routing_for_categories_services.html"
    }
});

        