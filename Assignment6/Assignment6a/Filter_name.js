//Enhancing the builtin filter to be able to filter even when the user changes the radio button values.


angular.module("myApp",[])

    .controller("StudentCtrl",function($scope) {
    
        $scope.student_list = 
            [
                {
                    'first_name':"Raj",
                    'last_name':"Dexter"
                },
                {
                    'first_name':"Manu",
                    'last_name':"Kandell"
                },
                {
                    'first_name':"Sangee",
                    'last_name':"Emilitee"
                },
                {
                    'first_name':"Priya",
                    'last_name':"Kripa"
                },
                {
                    'first_name':"Vaish",
                    'last_name':"Raja"
                },
                {
                    'first_name':"Arjun",
                    'last_name':"Raja"
                },
                {
                    'first_name':"Vishnu",
                    'last_name':"Priya"
                }
            ];
    
        
        $scope.student={"name":"first_name"};
    
        $scope.search={
                    "first_name":"",
                   "last_name":""
                  }
        
        $scope.$watch("student.name",function(val,ol){
            if(!val) return;
            
            if(val == "last_name")
            {
                $scope.name=$scope.search.first_name
                $scope.search={};
                $scope.search.last_name = $scope.name
            }
            else if(val == "first_name")
            {
             
                $scope.name=$scope.search.last_name
                $scope.search={};
                $scope.search.first_name = $scope.name
                
            }
        });

});