//The filter "Filter_name" is used to filtered the list basd on both first and last name 


angular.module("myApp",[])

    .controller("StudentCtrl",function($scope) {
    
        $scope.student_list = 
            [
                {
                    'first_name':"Raj",
                    'last_name':"Dexter"
                },
                {
                    'first_name':"Manu",
                    'last_name':"Kandell"
                },
                {
                    'first_name':"Sangee",
                    'last_name':"Emilitee"
                },
                {
                    'first_name':"Priya",
                    'last_name':"Kripa"
                },
                {
                    'first_name':"Vaish",
                    'last_name':"Raja"
                },
                {
                    'first_name':"Arjun",
                    'last_name':"Raja"
                },
                {
                    'first_name':"Vishnu",
                    'last_name':"Priya"
                }
            ];

    })



    .filter("filter_name",function() {
        return function(student_list,search)
        {
            if(!search)
                return student_list;
            var filtered = [];
            angular.forEach(student_list, function(student) {
                if( student.first_name.indexOf(search) >= 0 || student.last_name.indexOf(search) >= 0) 
                    filtered.push(student);
                });
            return filtered;      
        }
    });
            
            
            
            
            
            
            