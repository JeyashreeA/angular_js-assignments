//Making use of parsers to validate the password as the user enters into the input box


angular.module("myApp",[])

.controller("myCtrl",function($scope) {
    
    $scope.check = false;
    $scope.status = false;
    $scope.check2 = false;
    $scope.valuenum = false;
    
})

.directive("passwordvalidation",function() {
    return {
        
        require: 'ngModel',
        link: function(scope,ele,attrs,ctrl)
                {
                
                ctrl.$parsers.unshift( function(viewValue)
                    {
                        scope.check = /[\W]/.test(viewValue);
            
                        scope.status = /[a-z]/.test(viewValue);
            
                        if(!scope.check && !scope.status)
                        {
                            scope.check2 = /[0-9]/.test(viewValue);
                            scope.valuenum = /[A-Z]/.test(viewValue);
                        }
                    
                        if(scope.check || scope.status || !scope.check2 || !scope.valuenum)
                        {
                            console.log("setting1");
                            ctrl.$setValidity('pwd',false);
                        }
                        else
                        {
                             console.log("setting2");
                            ctrl.$setValidity('pwd',true);
                        }
                     
                    });
                
                }
        };
});