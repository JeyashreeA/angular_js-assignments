//Factory holds the list of books


var app = angular.module('myApp',[]);

app.factory("book",function() {
    var books = {};
    books.list = [ 
    {
        "id" : "0",
        "imageUrl": "images/comp_ref.jpg", 
        "book_name": "The Complete Reference for C++", 
        "author_name": "Herb Schildt",
        "price" : "699",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":4
    }, 
    {
        "id" : "1",
       "imageUrl": "images/catching_fire.jpg", 
        "book_name": "Catching Fire", 
        "author_name": "Suzzane collins",
        "price" : "499",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":3
    }, 
    { 
        "id" : "2",
        "imageUrl": "images/divergent.jpg", 
        "book_name": "Divergent", 
        "author_name": "Veronica Roth",
        "price" : "399",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":2
    }, 
    {
        "id" : "3",
        "imageUrl": "images/harry_potter.jpg", 
        "book_name": "Harry Potter and the Sorcerer's stone", 
        "author_name": "J.K.Rowling",
        "price" : "399",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":1
    },
    {
        "id" : "4",
        "imageUrl": "images/mean_machine.jpg", 
        "book_name": "The MEAN machine", 
        "author_name": "Chris Sevilleja",
        "price" : "699",
        "publisher" : "ABC publishers",
        "check" : true,
        "stock":0
    }
];
    return books;
});

//Controller keeps tabs on the availability of the books using function update()

app.controller('bookCtrl', function (book,$scope,$element) {
    $scope.books = book;
     $scope.update= function(bookId) {
         for (i in $scope.books.list)
         {
             if($scope.books.list[i].id == bookId)
                {
                  $scope.books.list[i].check=$scope.books.list[i].check == false?true:false;
                    if($scope.books.list[i].check == false)
                        $scope.books.list[i].stock--;
                    else
                        $scope.books.list[i].stock++;
                    console.log($scope.books.list[i].stock);
                }
        }
     }

});

//Directive including the Multiple_selection_directive.html

app.directive('bookdirective', function() {
    return {
        restrict: "E",
        templateUrl: "Multiple_selection_directives.html"

    }
});



