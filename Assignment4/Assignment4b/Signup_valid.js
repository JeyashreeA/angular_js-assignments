//Using watch to ensure that the password includes only lower case charcaters and numbers 


angular.module("myApp",['ngMessages'])

.controller("MyController",function( $scope ) {

        $scope.valuenum = false;
        $scope.status = false;
        $scope.check = false;
        $scope.check2 = false;
    
        $scope.$watch("password",function(val,oldvalue){    
            if(!val) return; 
        
            $scope.check = /[\W]/.test(val);
            
            $scope.status = /[a-z]/.test(val);
            
            if(!$scope.check && !$scope.status)
            {
                $scope.check2 = /[0-9]/.test(val);
                $scope.valuenum = /[A-Z]/.test(val);
            }
    })
});







































//.directive('validatePasswordCharacters', function() {
//
//  var REQUIRED_PATTERNS = [
//    /\d+/,    //numeric values
//    /[a-z]+/, //lowercase values
//    /[A-Z]+/, //uppercase values
//    /\W+/,    //special characters
//    /^\S+$/   //no whitespace allowed
//  ];
//
//  return {
//    require : 'ngModel',
//    link : function($scope, element, attrs, ngModel) {
//      ngModel.$validators.passwordCharacters = function(value) {
//        var status = true;
//        angular.forEach(REQUIRED_PATTERNS, function(pattern) {
//          status = status && pattern.test(value);
//        });
//        return status;
//      }; 
//    }
//  }
//});
//                
    