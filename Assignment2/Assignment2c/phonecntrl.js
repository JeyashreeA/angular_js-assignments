//Factory to store the list of phones and their details

var app = angular.module('myApp', []);

app.factory("phone", function () {
    var phones = {};
    phones.list = [
        {
            "imageUrl": "images/yureka.jpg",
            "name": "Yureka",
            "snippet": "The Zeus Of Phones",
            "camera" : "13 MP, 4208 x 3120 pixels, autofocus, LED flash",
            "wifi" : "Yes",
            "gps" : "Yes"
        },
        {
            "imageUrl": "images/one-plus-one.jpg",
            "name": "One Plus One",
            "snippet": "An experience to cheer about.",
            "camera" : "13 MP, 4128 x 3096 pixels, autofocus, dual-LED flash",
            "wifi" : "Yes",
            "gps" : "Yes"
        },
        {
            "imageUrl": "images/s6_edge.jpg",
            "name": "Samsung Galaxy S6 Edge",
            "snippet": "Are you ready for everything life throws your way?",
            "camera" : "16 MP, 2988 x 5312 pixels, optical image stabilization, autofocus, LED flash",
            "wifi" : "Yes",
            "gps" : "Yes"
        },
        {
            "imageUrl": "images/moto-2nd-gen.jpg",
            "name": "Moto G 2nd Generation",
            "snippet": "MotoG II gen!!",
            "camera" : "8 MP, 3264 x 2448 pixels, autofocus, LED flash",
            "wifi" : "Yes",
            "gps" : "Yes"
        }
    ];
    return phones;
});

//Controller to access list of phones from the factory injected into it

app.controller('phoneCtrl', function (phone) {
    this.phones = phone;
});


