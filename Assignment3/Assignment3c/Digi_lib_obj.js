//Each category having its own controller and a seperate variable "checkbutton" that is shared between the two via factories 
//the Controllers are accessed as objects by using "as"

angular.module("myApp", [])
    
.factory("check_button_func", function () {
         return {
            check_button : "false"
         };
         })
    
.controller("cntrl",function( $scope,check_button_func ) {
    var techCtrl = this;
	var books = [
        {
		  "Name": "Physics",
		  "topics": ['Nuclear Physics','Mechanics','Robotics']
        },
        
        {
            "Name": "Maths",
            "topics": ['integration','differntials','geometry']
        },
        
        {
            "Name": "Chemistry",
            "topics": ['Acids','Alkalis','base']
        }
	];
    techCtrl.check_button_func = check_button_func;
    techCtrl.checkout1 = "false";
	techCtrl.books = books;
	techCtrl.Techcategory = books[0];
    
    $scope.$watch (function () {
        return techCtrl.Techcategory;
    }, function(value, oldValue){
        techCtrl.currentBook = value.topics[0]; 
    }, true)
            
    techCtrl.onClick = function () {
         techCtrl.checkout1 = "true";   
        }
        
    techCtrl.onClickButton = function () {
         techCtrl.checkout1 = "false";  
         techCtrl.check_button_func.check_button = "true";
        }
        
//        return techCtrl.cntrl=this;

})


.controller("NonTechController",function( $scope,check_button_func ) {
    var nontechCtrl = this;
	var books = [
        {
		  "Name": "Thriller",
		  "topics": ['Hot Ice','60 days','You']
        },
        
        {
            "Name": "Fantasy",
            "topics": ['Harry Potter','Divergent','Hunger Games']
        },
        
        {
            "Name": "Detective",
            "topics": ['Sherlock Holmes','Deception Point','Digital Fortress']
        }
	];
    nontechCtrl.check_button_func = check_button_func;
    nontechCtrl.checkout1 = "false";
	nontechCtrl.books = books;
	nontechCtrl.nonTechCategory = books[0];
    
    $scope.$watch(function () {
        return nontechCtrl.nonTechCategory;
    }, function(value, oldValue){
        nontechCtrl.currentBook = value.topics[0]; 
    },true)
    
        nontechCtrl.onClick = function () {
         nontechCtrl.checkout1 = "true";   
        }
        
        nontechCtrl.onClickButton = function() {
         nontechCtrl.checkout1 = "false";   
         nontechCtrl.check_button_func.check_button = "true";
        }

});