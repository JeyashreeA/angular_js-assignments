//Each category having its own controller and a seperate variable "checkbutton" that is shared between the two via factories 

angular.module("myApp",[])
    
    .factory("check_button_func", function () {
        return {
            check_button : "false"
        };
    })
    
    .controller("Controller", function ($scope, check_button_func) {
    
	    var books = [
                {
		            "Name": "Physics",
		            "topics": ['Nuclear Physics', 'Mechanics', 'Robotics']
                },
        
                {
                    "Name": "Maths",
                    "topics": ['integration', 'differntials', 'geometry']
                },
        
                {
                    "Name": "Chemistry",
                    "topics": ['Acids', 'Alkalis', 'base']
                }
	        ];
        $scope.check_button_func = check_button_func;
        $scope.checkout1 = "false";
	    $scope.books = books;
	    $scope.Techcategory = books[0];
    
        $scope.$watch('Techcategory', function (value, oldValue) { //To update the second drop-Downlist based on the first one
            $scope.currentBook = value.topics[0];
    });
    
//    $scope.onClick = function () { //To enable the button
//         $scope.checkout1 = "true";   
//        }
        
    $scope.onClickButton = function() {
         $scope.checkout1 = "false";  
         $scope.check_button_func.check_button = "true";
        }

})


.controller("NonTechController",function( $scope,check_button_func ) {
    
	var books = [
        {
		  "Name": "Thriller",
		  "topics": ['Hot Ice','60 days','You']
        },
        
        {
            "Name": "Fantasy",
            "topics": ['Harry Potter','Divergent','Hunger Games']
        },
        
        {
            "Name": "Detective",
            "topics": ['Sherlock Holmes','Deception Point','Digital Fortress']
        }
	];
    $scope.check_button_func = check_button_func;
    $scope.checkout1 = "false";
	$scope.books = books;
	$scope.nonTechCategory = books[0];
    
    $scope.$watch('nonTechCategory', function(value, oldValue){
            $scope.currentBook = value.topics[0]; 
    });
        
    $scope.onClickButton = function () {
            $scope.checkout1 = "false";   
            $scope.check_button_func.check_button = "true";
        }

});













//    $scope.$watch('currentItem', function(value,oldvalue) {
//        $scope.checkout1 = "false";
//    });
//.directive('myDirective', function() {
//    return {
//      replace: true,
//      template: function(elem, attr) {
//          var newElem;
//          elem.bind("mouseclick",function(){
//            newElem = '<button ng-show="true">Checkout</button>';
//          });
//        return newElem;
//      }
//    };
//  });