//Each category having its own controller and a seperate variable "checkbutton" that is shared between the two via rootScope

angular.module("myApp",[])

.run(function($rootScope) {
    $rootScope.check_button = "false";
    console.log($rootScope.check_button);
})
    

.controller("Controller",function( $scope,$rootScope ) {
    
	var books = [
        {
		  "Name": "Physics",
		  "topics": ['Nuclear Physics','Mechanics','Robotics']
        },
        
        {
            "Name": "Maths",
            "topics": ['integration','differntials','geometry']
        },
        
        {
            "Name": "Chemistry",
            "topics": ['Acids','Alkalis','base']
        }
	];

    $scope.checkout1 = "false";
	$scope.books = books;
	$scope.Techcategory = books[0];

    
    $scope.$watch('Techcategory', function (value, oldValue) {
        $scope.currentBook = value.topics[0]; 
    });
    
   
    $scope.onClickButton = function() {
         $scope.checkout1 = "false";  
         $rootScope.check_button = "true";
        console.log($rootScope.check_button);
        }

})


.controller("NonTechController",function( $scope,$rootScope ) {
    
	var books = [
        {
		  "Name": "Thriller",
		  "topics": ['Hot Ice','60 days','You']
        },
        
        {
            "Name": "Fantasy",
            "topics": ['Harry Potter','Divergent','Hunger Games']
        },
        
        {
            "Name": "Detective",
            "topics": ['Sherlock Holmes','Deception Point','Digital Fortress']
        }
	];

    $scope.checkout1 = "false";
	$scope.books = books;
	$scope.nonTechCategory = books[0];
    
    
    $scope.$watch('nonTechCategory', function(value, oldValue){
            $scope.currentBook = value.topics[0]; 
    });

   
    $scope.onClickButton = function () {
            $scope.checkout1 = "false";   
            $rootScope.check_button = "true";
       
        }

});

