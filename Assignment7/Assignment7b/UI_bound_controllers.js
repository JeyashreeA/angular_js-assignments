var app = angular.module('app', []);
 
 app.controller("firstCtrl", function ($scope) {
    $scope.moving_avg = function () {
        $scope.list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        $scope.nums = [];
        $scope.result = [];
        for (var i in $scope.list) {
         $scope.num = $scope.list[i];
            $scope.nums.push($scope.num);
        if ($scope.nums.length > 5)
            $scope.nums.splice(0,1);// remove the first element of the array
        if ($scope.nums.length == 5)
        {
         var sum = 0;
        for (var i in $scope.nums)
            sum += $scope.nums[i];
        var n = 5;
        if ($scope.nums.length < n)
            n = $scope.nums.length;
        $scope.result.push(sum/n);
        }
        
     }
     $scope.check = true;
     $scope.broadcast_method($scope.result);
 }
 
 
 $scope.broadcast_method = function(msg)
 {
     $scope.$broadcast('eventName', { message: msg });
 }
 
 
 });
 
 app.controller("secondCtrl", function ($scope) {
     
 $scope.$on('eventName', function (event, args) {
    $scope.result = args.message;
    console.log($scope.result);    
 });
     
});