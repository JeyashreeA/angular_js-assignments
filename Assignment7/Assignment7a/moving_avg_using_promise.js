var app = angular.module("myApp", []);

app.service('validateService', [ '$q', '$timeout',
    function validateService($q, $timeout) {
        var validateService = {
            nums: []
        };

        // implementation
        validateService.validate_length = function (i) {
            var defer = $q.defer();
            var notifications = 0;
            
            for (var k = 0; k <= 2000 / 1000; k++) {
                
            $timeout(function() {
                
                defer.notify(notifications++);
            }, 2000);
        }
            
            $timeout(function () {
                
                if (i < 10) {
                    defer.resolve("Good");
                } else if(i == 10) {
                    defer.reject("input data over!!");
                    console.log("rejecting");
                } 
            }, 2000);

            return defer.promise;
        };
        
        validateService.mov_avg = function (num, i) {
            var nums = [];
            var n = 5, a = 0, sum = 0, k;
                
              
            while (a < n && i <= 9) {
                nums.push(num[i]);
                a++;
                i++;
            }
           
            if (nums.length > n) {
                nums.splice(0, 1);
            }// remove the first element of the array
        
            for (k in nums) {
                sum += nums[k];
            }
            
            if (nums.length < n) {
                n = nums.length;
            }
         
            if (i == 10) {
                console.log("comin in");
                validateService.validate_length(i).then(function () {}, function (string) {
                    alert("error: " + string);
                });
            }
            return (sum / n);
        };
        
        return validateService;
    }]);

app.controller('myController', [
    '$scope', 'validateService',
    function myController($scope, validateService) {
        var vm = this;
        //vm.albums = [];
        vm.list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        vm.avg = [];
        vm.index_val = 0;
        vm.notify = 0;
        var i = 0;
        vm.validate_length = function () {
            
            validateService.validate_length(vm.index_val)
                .then(function (string) {
                
                    if (string === "Good") {
                        vm.limit = vm.list.length / 2;
                        
                        while (i <= vm.limit) {
                            vm.index_val = i;
                            vm.avg_val = validateService.mov_avg(vm.list, vm.index_val);
                            vm.avg.push(vm.avg_val);
                            i++;
                        }
                    $scope.check = true;  
                    }
                }, function () {}, function(value) {
                    vm.notify = value;
            });
            
        };
            
    }
    
]);