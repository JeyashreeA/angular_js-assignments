//ng-app to implement the angular

angular.module("myApp", ['ngRoute'])

//Factory is used to stire the details about the choosen bus travels
    .factory("Choosen", function () {

        var choosen = []; //stores the final choosen travels details
        return choosen;
        
    })

//the routeProvider is used to route to the different templates
    .config(["$routeProvider", function ($routeProvider) {
        $routeProvider
            .when('/search',                          //Route to search_bus.html 
                {
                    templateUrl : "HTML_files/search_bus.html",
                    controller : "searchBusCtrl"
                })
            .when('/view',                            //Route to view_seats.html 
                {
                    templateUrl : "HTML_files/view_seats.html",
                    controller : "searchBusCtrl"
                })
            .when('/issueTicket',                      //Route to issue_ticket.html 
                {
                    templateUrl : "HTML_files/issue_ticket.html",
                    controller : "issueTicketBusCtrl"
                });
    }])


/*Controller myBusCtrl gets the list of places the buses will ply to using $http and also sets the minimum date for the datepicker*/

    .controller("myBusCtrl", ["$scope", "$http", "$window", function ($scope, $http, $window) {
        $scope.choose = true;                    //variable to control the display of div element
        $scope.place_list = {};                  //Consists of the routes to which the buses go
        $scope.mindate = new Date();
        $http.get("JSON_files/place_list.json").success(function (response) {
            $scope.place_list = response.list;
        });
        
        $scope.searchBuses = function () {            //function to set the choose variable and change location
            $scope.choose = false;
            //$location.path('/search');
            $window.location = "#search";
        };
        
    }])

/*Controller searchBusCtrl is used to search through the JSON files and also store the appropriate travels that the customer wants*/
    .controller("searchBusCtrl", ["$scope", "$http", "$window", "Choosen", function ($scope, $http, $window, Choosen) {
        
        $scope.sorted_list = [];               //to store the details of travels in the specified route
        $scope.total_cost = "";                //to determine the total cost
        $scope.choosen = Choosen;              //to store the final selected travels details
        $scope.seat_list = [];
        $scope.$watch('Source', function (value, oldValue) {
            $scope.scr = value.source;
        });
        
        $scope.$watch('Destination', function (value, oldValue) {
            $scope.dest = value;
        });
        
        $scope.$watch('date_test', function (value, oldValue) {
            $scope.date = value;
        });
        
        $http.get("JSON_files/bus_travels.json").success(function (response) {
            $scope.travel_list = response.list;              //travel_list stores the list of all buses in all routes
            $scope.search_bus();
        });
        
        $scope.search_bus = function () {            //To search for travels in the specified route

            var i = 0;
            
            for (i in $scope.travel_list) {
                if ($scope.travel_list[i].source == $scope.scr && $scope.travel_list[i].destination == $scope.dest) {
                    $scope.sorted_list.push($scope.travel_list[i]);
                }
            }
        };
        
        $scope.view_seats = function (travels_name) {        //To store the details of the final choosen travels
            
            var i = 0;
        
            for (i in $scope.sorted_list)
                {
                    if ($scope.sorted_list[i].travels_name == travels_name)
                        $scope.choosen.push($scope.sorted_list[i]);
                }
            console.log($scope.choosen);
            //$location.path('/view');
            $window.location = "#view";
        };
        
        $scope.total_fare = function () {                      //To calculate the total amount to be paid
            
            $scope.total_cost = $scope.choosen[0].fare * $scope.number_of_seats;
        };
        
        $scope.revert_back = function () {                    //To enable the user to choose a different travels before confirmation
            
            $scope.choosen.pop();
            //$location.path('/search');
            $window.location = "#search";
        };
        
        $scope.confirm_tickets = function () {   //To store the details of the customer alsong wit travels company details for ticket generation
//            $scope.choosen.forEach(function (confirmation) {
                $scope.choosen.push({"name" : $scope.cust_name,
                                    "gender" : $scope.gender,
                                    "number_of_seats" : $scope.number_of_seats,
                                    "total_fare" : $scope.total_cost});
            console.log($scope.choosen);
//            });
            //$location.path('/issueTicket');
            $window.location = "#issueTicket";
        };
        
        $scope.generate_seats = function () {
            var i = 0;
            var no_of_seats = $scope.choosen[0].seats;
            for (i=0;i < no_of_seats;i++) {
                console.log("looping" + i);
                $scope.seat_list.push("S" + i);
            }
            console.log(i);
            console.log($scope.seat_list);
            console.log(0 < 30)
            console.log(i < no_of_seats);
            console.log(no_of_seats);
        };
        
       
    }])

/*Controller issueTicketBusCtrl is used to display the confirmed ticket details */
    .controller("issueTicketBusCtrl", ["$scope", "Choosen", function ($scope, Choosen) {
        $scope.confirmed = Choosen;             //To store the details of the final confirmed travels and customer details
        $scope.validity = false;                //Variable to ensure the user doesnt revert back to previous page after booking the tickets
        $scope.$on('$locationChangeStart', function (event, next, current) {
                                                //function to ensure the user doesnt revert back to previous page after booking the tickets
            $scope.validity = true;
            event.preventDefault();
            });
    }])

/*The Directive will include the template welcome.html that will use the let the user select the from, to and date of travel*/

    .directive("busDirective", function () {
        
        return {
            restrict : "E",
            templateUrl : "HTML_files/welcome.html",
            controller : "myBusCtrl"
        };
        
    });
        
        