'use strict';

angular.module('fullstackApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'uiGmapgoogle-maps',
  'duScroll'
])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/final_itinerary', {
            templateUrl: 'app/main/final_itinerary.html',
            controller: 'ItineraryCtrl'
    });
//       .when('/save_as_pdf', {
//             templateUrl:'app/main/save_as_pdf.html',
//             controller: 'ItineraryCtrl'
//    });
//      .otherwise({
//        redirectTo: '/'
//      });

    $locationProvider.html5Mode(true);
  });