'use strict';

angular.module('fullstackApp')
    .controller('MainCtrl', function ($scope, $http, $document, itinerary, trip_details) {  //controller for handling the entire process of displaying the result
        $scope.map = {                                             //Contains the initial properties of the map.
                        center: {
                            latitude: 40.1451,
                            longitude: -99.6680
                        },
                        zoom: 4
                    };
        $scope.options = {                                         //enabling scrollwheel functionality on map
            scrollwheel: true
        };
        $scope.selected_toggle = false;
        $scope.mindate = new Date();
        $scope.choose_place = false;
        $scope.itinerary = itinerary;
        $scope.trip_details = trip_details;
        $scope.push_dates = function () {
            var i, oneDay = 24*60*60*1000;
            $scope.no_of_days = Math.round(Math.abs(($scope.from_date.getTime() - $scope.to_date.getTime())/(oneDay)));
            $scope.no_of_days = $scope.no_of_days + 1;
            for (i = 0; i < $scope.no_of_days; i++) {
                var j = i + 1;
                $scope.itinerary.push (
                                        {
                                            "id": [],
                                            "day": j,
                                            "place": [],
                                            "address": []
                                        }
                    )
            }
        };
    
        $scope.selected_place = function (id) {        //Function to push and pop the places of interest into the final itinerary list
            console.log("am here no worries!! :)");
            var i;
            if($scope.marker[$scope.selected_place_id].place_selected) {
                for (i = 0; i < $scope.no_of_days; i++) {
                    if(i == id) {
                        $scope.itinerary[id].place.push($scope.place_name);
                        $scope.itinerary[id].address.push($scope.address);
                        $scope.itinerary[id].id.push($scope.photo_id);
                        $scope.marker[$scope.selected_place_id].scheduled_on = i;
                        console.log($scope.itinerary);
                        return;
                    }
                }
            }
            else {
                for (i = 0; i < $scope.no_of_days; i++) {
                    if(i == id) {
                        var index = $scope.itinerary[id].place.indexOf($scope.place_name);
                        if(index == 0)
                            $scope.itinerary[id].place.splice(index, 1);
                        return;
                    }
                }
            }
            
        };
                
        $scope.decided_place = function(id, name) {          //To set variables that will toggle the display based on the place choosen
            var i, k;
            $scope.place_name = $scope.response.response.groups[0].items[id].venue.name;
            $scope.address = $scope.response.response.groups[0].items[id].venue.location.formattedAddress.toString();
            $scope.photo_id = $scope.response.response.groups[0].items[id].venue.id;
            $scope.marker[id].place_selected = $scope.marker[id].place_selected == true?false:true;
            $scope.selected_place_id = id;
        }
    
        $scope.choose_place_func = function () {      //To help toggle between the divs using ng-switch
            $scope.choose_place = true;
        };
    
        var events = {
            places_changed: function (searchBox) {      //to include the events in the map directive
                $scope.place = searchBox.getPlaces();   //Using google API function
                if (!$scope.place || $scope.place == 'undefined' || $scope.place.length == 0) {
                    console.log('no place data :(');
                    return;
                }

                $scope.map = {                          //Setting the map on the location specified by user
                    "center": {
                        "latitude": $scope.place[0].geometry.location.lat(),
                        "longitude": $scope.place[0].geometry.location.lng()
                    },
                    "zoom": 14
                };
                $scope.marker =                         //Setting the marker on the location specified by user
                    [
                        {
                            id: 0,
                            coords: {
                                latitude: $scope.place[0].geometry.location.lat(),
                                longitude: $scope.place[0].geometry.location.lng()
                            },
                            message: $scope.place[0].formatted_address,
                            windowOptions : {
                                id: 0,
                                visible: false
                            },
                            rating: "NADA ;)"
                        }
                    ];

                $scope.onClick = function (id) {             //To create the infowindows when clicked on marker
                    var k, i;
                    for (k = 0; k < $scope.marker.length; k++) {
                        $scope.marker[k].windowOptions.visible = false;
                    }
                    for (i = 0; i < $scope.marker.length; i++) {
                        if (id === $scope.marker[i].id) {
                            $scope.marker[i].windowOptions.visible = true;
                            return;
                        }
                    }
                };    
 
                $scope.closeClick = function (id) {
                    $scope.marker[id].windowOptions.visible = false;
                };   //To close the infowindow
                
                $scope.address = $scope.place[0].formatted_address;
                
                $http.get("https://api.foursquare.com/v2/venues/explore?near=" + $scope.address + "&oauth_token=CFXSCBH3Z03TNU41CWZ1BOK0PRPPCQTPOM5OUHYQ2O1533FG&v=20150528")  //get the content from foursquare api
                    .success(function (response) {
                        $scope.response = response;
                        $scope.place_available = true;
                        $scope.populate_venues();
                    })
                    .error(function (error) {
                        $scope.response = error;
                        $scope.place_available = false;
                    });
            }
        };
    
        $scope.populate_venues = function () {        //to create markers for the attractions populated
            $scope.attractions = $scope.response.response.groups[0].items.length;
            var j = 1;
            for (var i in $scope.response.response.groups[0].items) { 
                $scope.marker.push(
                                    {
                                        "id":j,
                                        "coords": {
                                            latitude:$scope.response.response.groups[0].items[i].venue.location.lat,
                                            longitude: $scope.response.response.groups[0].items[i].venue.location.lng
                                        },
                                        "message": $scope.response.response.groups[0].items[i].venue.name,
                                        "windowOptions": {  
                                            visible: false,
                                            id: j
                                        },
                                        "rating": $scope.response.response.groups[0].items[i].venue.rating,
                                        "place_selected":false,
                                        "scheduled_on":0
                                    });
                j++;
            }
            
            $scope.trip_details.push(
                                      {
                                          "from_date": $scope.from_date,
                                          "to_date": $scope.to_date,
                                          "destination": $scope.marker[0].message
                                      });
            console.log($scope.trip_details);
        };
      
        $scope.setSelected = function(index) {      //To enable binding between the place details in container and the marker
            $scope.idSelected = index;
            
            var i, k;
            for (i = 0; i < $scope.attractions; i++) {
                if ($scope.idSelected == $scope.marker[i].windowOptions.id) {
                    var j = i + 1;
                    for(k = 0; k < $scope.marker.length; k++) {
                        $scope.marker[k].windowOptions.visible = false;
                    }
                    $scope.marker[j].windowOptions.visible=true;
                    $document.scrollTopAnimated(0, 1000)
                    $scope.map.center.latitude=$scope.response.response.groups[0].items[i].venue.location.lat;
                    $scope.map.center.longitude=$scope.response.response.groups[0].items[i].venue.location.lng;
                }
            }
        };

        $scope.searchbox = { template:'searchbox.tpl.html', events:events};
    })

.controller("ItineraryCtrl", function ($scope, $http, trip_details, itinerary) {
    $scope.itinerary = itinerary;

    $scope.trip_details = trip_details;
    
    $scope.uploadClick = function() {
        var doc = new jsPDF('p', 'pt', 'letter');        
         var specialElementHandlers = {
             '#bypassme': function(element, renderer) {
                 return true;
             }
         }, 
        margins = {top:50, left:60, width:545};
        doc.fromHTML ($('#final').get(0), margins.left, margins.top, {
                 'width': margins.width, // max width of content on PDF
                 'elementHandlers': specialElementHandlers
             });
        doc.save('itinerary.pdf');

    }
////    $scope.get_photos = function() {
////        var i;
////        console.log($scope.itinerary.id.length);
////        for(i = 0; i < $scope.itinerary.id.length; i++) {
////            $http.get("https://api.foursquare.com/v2/venues/"+$scope.itinerary.id[i]+"/photos?oauth_token=CFXSCBH3Z03TNU41CWZ1BOK0PRPPCQTPOM5OUHYQ2O1533FG&v=20150608")
////                .success(function (response) {
////                    $scope.photo_list.push(response.prefix + "original" + response.suffix);
////                }).error(function (response) {
////                    $scope.error("no photos available");
////            });
////        }
//        
//        console.log($scope.photo_list);
//
//     }
//    $scope.get_photos();
});
