'use strict';

angular.module('fullstackApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      });
//        .when('/final_itinerary', {
//            templateUrl: 'app/main/final_itinerary.html',
//            controller: 'MainCtrl'
//    });
  })

    .config(function(uiGmapGoogleMapApiProvider) {
      uiGmapGoogleMapApiProvider.configure({
          //    key: 'your api key',
          v: '3.17',
          libraries: 'places' // Required for SearchBox.
      });
})

    .run(['$templateCache', function ($templateCache) {
  $templateCache.put('searchbox.tpl.html', '<input id="pac-input" class="pac-controls" type="text" ng-model="ngModel" placeholder="Search for your Place">');
}])

    .factory("itinerary", function () {       
        var itinerary = [
//            {
//                day:1,
//                place:['abc','def']
//            },
//            {
//                day:2,
//                place:['abc1','def1']
//            },
//            {
//                day:3,
//                place:['abc2','def2']
//            },
//            {
//                day:4,
//                place:['abc3','def3']
//            },
//            {
//                day:5,
//                place:[]
//            }
//        
        ];
        return itinerary;
})

    .factory("trip_details", function() {
        var trip_details = [];
    return trip_details;
});

